module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        "primary": {
          "50": "#b87dff",
          "100": "#ae73ff",
          "200": "#a469ff",
          "300": "#9a5fff",
          "400": "#9055ff",
          "500": "#864bff",
          "600": "#7c41f5",
          "700": "#7237eb",
          "800": "#682de1",
          "900": "#5e23d7"
        }
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}

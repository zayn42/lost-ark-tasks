
import categorys from "@/assets/data/categorys"

// Char Daily
const charDailyCategories = categorys.charDailyCategorys as const
type CharDailyCategories = typeof charDailyCategories[number]

// Char Weekly
const charWeeklyCategories = categorys.charWeeklyCategorys as const
type CharWeeklyCategories = typeof charWeeklyCategories[number]

interface Category {
    label: string
    name: CharDailyCategories | CharWeeklyCategories
}

interface Task {
    label: string
    name: string,
    // category: CharDailyCategories[] | CharWeeklyCategories[]
    category: string
}

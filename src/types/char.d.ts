interface CharProfile {
    name: string
    class: string
    categories: Array[] | Booleanish
}

interface Char extends CharProfile {
    id: string,
    dailies: Array[] | Booleanish
    weeklies: Array[] | Booleanish
}

// interface AnyObject {
//     [key: string]: any
// }
import { acceptHMRUpdate, defineStore } from "pinia"

export const useSettingsStore = defineStore("settings", () => {
    // Theme
    const theme = ref<Theme>("dark")

    function setTheme(value: Theme) {
        document.documentElement.classList.remove(theme.value)
        theme.value = value
        document.documentElement.classList.add(value)
    }

    // Const 
    const crystallineAura = ref<boolean>(true)

    return {
        theme,
        setTheme,

        crystallineAura,
    }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useSettingsStore, import.meta.hot))
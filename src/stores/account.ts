
import { acceptHMRUpdate, defineStore } from "pinia"
// import { Char, CharProfile, Task } from "@/types/char"
import dataTasks from "@/assets/data/tasks.json"
import { nanoid } from 'nanoid'


export const useAccountStore = defineStore("account", () => {

    // Characters
    const chars = ref(new Array<Char>())
    function addChar(char: CharProfile): void {

        const dailies = ref(new Array())
        const weeklies = ref(new Array())

        chars.value.push({ ...char, dailies, weeklies, id: nanoid(4) })
    }

    function removeChar(id: string): void {
        const index = chars.value.findIndex(function (char) {
            return char.id === id;
        })
        if (index !== -1)
            chars.value.splice(index, 1)
        // chars.value.indexOf()
        // const char = chars.value.map((val: Char) => val.id === id)
        // if (char)

        // return props.modelValue.filter((val: string) => val === name).length > 0
        // console.log(char)
        // chars.value.splice(index, 1);
    }

    // Account Tasks
    const dailies = ref(new Array())
    const weeklies = ref(new Array())
    const rapports = ref(new Array())
    const tasks = ref({ dailies, weeklies, rapports })

    // Reset Function (Daily)
    function resetDailies(): void {
        chars.value = chars.value.map((char) => {
            const dailies = new Array()
            return { ...char, dailies }
        })
        tasks.value.dailies = new Array()
        tasks.value.rapports = new Array()
    }

    // Reset Function (Weekly)
    function resetWeeklies(): void {
        chars.value = chars.value.map((char) => {
            const weeklies = new Array()
            return { ...char, weeklies }
        })
        tasks.value.weeklies = new Array()
    }

    // Reset Function (All)
    function resetAllTasks(): void {
        resetDailies()
        resetWeeklies()
    }

    // Tasks in Percentage
    // TODO: Calc nicer & correctly!
    const taskPercent = computed((): number => {

        // Characters
        const charTasksDone = chars.value.reduce(
            (val, char) => val + char.dailies.length + char.weeklies.length,
            0
        )
        const charTasksMax = chars.value.reduce(
            (val, currentValue) => val + dataTasks.charDailies.length + dataTasks.charWeeklies.length,
            0
        )

        // Dones Account Tasks
        const accountTasksDone = Object.values(tasks.value).flat().length

        // Max Tasks
        const accountTasksMax = dataTasks.accountDailies.length + dataTasks.accountRapport.length + dataTasks.accountWeeklies.length

        return Math.floor((charTasksDone + accountTasksDone) / (charTasksMax + accountTasksMax) * 100)
    })

    function deleteData() {
        chars.value = new Array<Char>()
        tasks.value.dailies = new Array()
        tasks.value.weeklies = new Array()
        tasks.value.rapports = new Array()
        localStorage.removeItem('state')
    }


    return {
        chars,
        addChar,
        removeChar,
        deleteData,
        resetDailies,
        resetWeeklies,
        resetAllTasks,
        taskPercent,
        tasks
    }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useAccountStore, import.meta.hot))

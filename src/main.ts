import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import './assets/styles/styles.css'

// Store
const pinia = createPinia()
if (localStorage.getItem("state")) {
    pinia.state.value = JSON.parse(localStorage.getItem("state")!)
}

watch(
    pinia.state,
    (state) => {
        localStorage.setItem("state", JSON.stringify(state))
    },
    { deep: true },
)

const app = createApp(App)
app.use(pinia)
app.mount('#app')
